// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
//  прототипне наслідування працює таким чином, що якщо у батьківськгого об'єкту відбуваються зміни, то чайлд їх успадковує.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
//  супер викаємо щоб спрацував батьківський консруктор, інакше this не буде створенний
console.log("test");
class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }

  set name(value) {
    if (value.length > 3) {
      this._name = value;
    }
  }

  get age() {
    return this._age;
  }

  set age(value) {
    if (typeof value === "number") {
      this._age = value;
    }
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    if (typeof value === "number") {
      this._salary = value;
    }
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get lang() {
    return this._lang;
  }
  set lang(value) {
    if (typeof value === "string") {
      this._lang = value;
    }
  }

  get salary() {
    return this._salary * 3;
  }
}

let employee1 = new Employee("Dima", 23, 1000);

let programmer = new Programmer("piter", 25, 1000, "c++");

console.log(programmer.salary);
