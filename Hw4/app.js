// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX - дозволяє завантажувати данні без оновлення сторінки

// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі.
// Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми,
// відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду,
// назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

const url = "https://ajax.test-danit.com/api/swapi/films";
const urlPeople = "https://ajax.test-danit.com/api/swapi/people/";
const div = document.querySelector(".container");
// function sendRequest() {
//   fetch(url)
//     .then((response) => response.json())
//     .then((data) => {
//       console.log(data);
//     });
// }

async function sendRequest(url) {
  const response = await fetch(url);
  return await response.json();
}

Promise.all([sendRequest(url), sendRequest(urlPeople)]).then(
  ([films, people]) => {
    films.forEach(({ name, episodeId, openingCrawl, characters }) => {
      const person = people.filter((item) => {
        if (characters.includes(item.url)) {
          return true;
        }
      });
      console.log(person);
      const personName = person.map((el) => {
        return el.name;
      });

      div.insertAdjacentHTML(
        "afterbegin",
        `<h2>${name}, ${episodeId}</h2>
          <p>${openingCrawl}</p>
          <p>${personName}</p>

      `
      );
    });
  }
);
