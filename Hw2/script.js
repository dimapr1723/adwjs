// 1) Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// 2) На сторінці повинен знаходитись div з id="root",
// куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// 3) Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність
// (в об'єкті повинні міститися всі три властивості - author, name, price).
// Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// 4) Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

let container = document.querySelector("#root");

const arr = ["author", "name", "price"];

books.forEach((e) => {
  if (Object.keys(e).length === arr.length) {
    let li = document.createElement("li");
    let list = document.createElement("ul");
    li.innerText = JSON.stringify(e);
    list.appendChild(li);
    container.append(list);
  } else {
    try {
      let errors = arr.filter((el) => {
        return !Object.keys(e).includes(el);
      });
      throw new Error(`Не вистачає таких властивостей в об'єкті ${errors}`);
    } catch (e) {
      console.error(e.message);
    }
  }
});
